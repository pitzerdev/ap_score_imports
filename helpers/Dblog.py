from pzlog.PzLog import Error, Warn, Fatal, Info


class Dblog(object):

    _title = ''
    _params = dict()
    _ex = None
    _type = 'info'
    _env = 'prod'
    _get_sess = True

    @staticmethod
    def err(ex, title, get_sess=True, env='prod'):
        return Dblog(ex, title, 'error', get_sess, env)

    @staticmethod
    def inf(data, title, get_sess=True, env='prod'):
        return Dblog(data, title, 'info', get_sess, env)

    @staticmethod
    def warn(ex, title, get_sess=True, env='prod'):
        return Dblog(ex, title, 'warn', get_sess, env)

    @staticmethod
    def fatal(ex, title, get_sess=True, env='prod'):
        return Dblog(ex, title, 'fatal', get_sess, env)

    def __init__(self, data, title, get_sess, type='info', env='prod'):
        self._data = data
        self._title = title
        self._env = env
        self._type = type

    def add(self, pname, p):
        self._params[pname] = p
        return self

    def ok(self):
        if self._get_sess:
            if '__session_info' in self._params:
                self._params['__session_info'] = self._get_session()

        if self._type == 'error':
            return Error(self._env, self._title, self._data, self._params)
        elif self._type == 'warn':
            return Warn(self._env, self._title, self._data, self._params)
        elif self._type == 'fatal':
            return Fatal(self._env, self._title, self._data, self._params)
        else:
            return Info(self._env, self._title, self._data, self._params)

    def _get_session(self):
        return ''