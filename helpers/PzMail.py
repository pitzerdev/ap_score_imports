import smtplib
from email.mime.text import MIMEText


class PzMail(object):
    host = 'smtp.pitzer.edu'

    @staticmethod
    def send(_from, to, subject, body, body_type='plain'):

        msg = MIMEText(body, body_type)
        msg['Subject'] = subject
        msg['From'] = _from
        msg['To'] = to
        msg.preamble = subject

        s = smtplib.SMTP(PzMail.host)
        s.sendmail(_from, to, msg.as_string())
        s.quit()


