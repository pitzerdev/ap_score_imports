from ldap3 import Server, ALL, Connection, SUBTREE

from helpers.Dblog import *


class AdConn(object):

    server = None

    @staticmethod
    def init(ad_info):
        AdConn.server = ad_info

    @staticmethod
    def conn(cb):
        conn = None
        ad_info = AdConn.server

        try:
            srv = Server(ad_info['server'], get_info=ALL, use_ssl=True)
            conn = Connection(srv,
                              ad_info['user'],
                              ad_info['password'],
                              ad_info['autobind'])
            return cb(conn)

        except Exception as ex:
            title = "[AdConn] Error when connecting to AD"
            Dblog.err(ex, title) \
                .add('ad_info', ad_info) \
                .add('conn', conn) \
                .ok()
            print(title)
            print(conn)
            raise

        finally:
            if conn is not None:
                conn.unbind(controls=None)

    @staticmethod
    def cn_exists(cn_prefix, ou='WDNew'):

        def _m(c):
            c.search(
                search_base=f'OU={ou},DC=pitzer,DC=edu',
                search_filter=f'(cn={cn_prefix}*)',
                search_scope=SUBTREE,
                attributes=['cn'],
                paged_size=10000
            )
            res = []

            for entry in c.response:
                res.append(entry['dn'])

            return res

        return AdConn.conn(_m)

    @staticmethod
    def dn_by_cxid(cxid, ou=None, attributes=None):
        if attributes is None:
            attributes = ['cn', 'mail', 'userPassword', 'userPrincipalName', 'sAMAccountName']

        def _m(c):
            res = []

            if ou is None:
                c.search(
                    search_base="DC=pitzer,DC=edu",
                    search_filter=f"(|(cXidPri={cxid}))",
                    search_scope=SUBTREE,
                    attributes=attributes,
                    paged_size=10000
                )

            else:
                c.search(
                    search_base=f"DC=pitzer,DC=edu",
                    search_filter=f"(|(cXidPri={cxid},ou={ou}))",
                    search_scope=SUBTREE,
                    attributes=attributes,
                    paged_size=10000
                )

            for entry in c.response:
                if 'attributes' in entry and 'sAMAccountName' in entry['attributes']:
                    res.append([entry['dn'], entry['attributes']])

            return res
        return AdConn.conn(_m)

    @staticmethod
    def dn_by_sAMAccount(sam_acc, ou=None, attributes=None):
        if attributes is None:
            attributes = ['cn', 'mail', 'userPassword', 'userPrincipalName', 'sAMAccountName']

        def _m(c):
            if ou is None:
                c.search(
                    search_base="DC=pitzer,DC=edu",
                    search_filter=f"(|(sAMAccountName={sam_acc}))",
                    search_scope=SUBTREE,
                    attributes=attributes,
                    paged_size=10000
                )

            else:
                c.search(
                    search_base=f"DC=pitzer,DC=edu",
                    search_filter=f"(|(sAMAccountName={sam_acc},ou={ou}))",
                    search_scope=SUBTREE,
                    attributes=attributes,
                    paged_size=10000
                )

            for entry in c.response:
                if 'attributes' in entry and 'sAMAccountName' in entry['attributes']:
                    return [entry['dn'], entry['attributes']]

            return None

        return AdConn.conn(_m)

    @staticmethod
    def dn_by_sAMAccounts(sam_acc, ou=None, attributes=None):
        if attributes is None:
            attributes = ['cn', 'mail', 'userPassword', 'userPrincipalName', 'sAMAccountName']

        def _m(c):
            res = []

            if ou is None:
                c.search(
                    search_base="DC=pitzer,DC=edu",
                    search_filter=f"(|(sAMAccountName={sam_acc}))",
                    search_scope=SUBTREE,
                    attributes=attributes,
                    paged_size=10000
                )

            else:
                c.search(
                    search_base=f"DC=pitzer,DC=edu",
                    search_filter=f"(|(sAMAccountName={sam_acc},ou={ou}))",
                    search_scope=SUBTREE,
                    attributes=attributes,
                    paged_size=10000
                )

            for entry in c.response:
                if 'attributes' in entry and 'sAMAccountName' in entry['attributes']:
                    res.append([entry['dn'], entry['attributes']])

            return res

        return AdConn.conn(_m)

    @staticmethod
    def dn_by_wdid(wdid, ou=None, attributes=None):
        if attributes is None:
            attributes = ['cn', 'mail', 'userPrincipalName', 'sAMAccountName']

        def _m(c):
            res = []

            if ou is None:
                c.search(
                    search_base="DC=pitzer,DC=edu",
                    search_filter=f"(|(pager={wdid}))",
                    search_scope=SUBTREE,
                    attributes=attributes,
                    paged_size=10000
                )

            else:
                c.search(
                    search_base=f"DC=pitzer,DC=edu",
                    search_filter=f"(|(pager={wdid},ou={ou}))",
                    search_scope=SUBTREE,
                    attributes=attributes,
                    paged_size=10000
                )

            for entry in c.response:
                if 'attributes' in entry and 'sAMAccountName' in entry['attributes']:
                    res.append([entry['dn'], entry['attributes']])

            return res
        return AdConn.conn(_m)

    @staticmethod
    def dn_by_email_prefix(email_prefix, ou=None, attributes=None):
        if attributes is None:
            attributes = ['cn', 'mail', 'userPrincipalName', 'sAMAccountName']

        def _m(c):
            res = []

            _prefix = f'{email_prefix}*@pitzer.edu'

            if ou is None:
                c.search(
                    search_base="DC=pitzer,DC=edu",
                    search_filter=f"(|(mail={_prefix}*))",
                    search_scope=SUBTREE,
                    attributes=attributes,
                    paged_size=10000
                )

            else:
                c.search(
                    search_base=f"DC=pitzer,DC=edu",
                    search_filter=f"(|(mail={_prefix}*,ou={ou}))",
                    search_scope=SUBTREE,
                    attributes=attributes,
                    paged_size=10000
                )

            for entry in c.response:
                if 'attributes' in entry and 'sAMAccountName' in entry['attributes']:
                    res.append([entry['dn'], entry['attributes']])

            return res
        return AdConn.conn(_m)
