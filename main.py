import hashlib
import json
import random
import sys

import urllib3

from db.Db import Db
from helpers.Dblog import Dblog

__config = 'config.json'
__download_if_new = True  # only stores content if its different than previous download


def initialize():
    try:
        data = json.load(open(__config))

    except Exception as ex:
        Dblog.err(ex, '[pyfetch init config] error while loading the config file') \
            .add('__config', __config) \
            .ok()
        print(f'[main.py config.json] there was an error while trying to read the config.json file: `${__config}`')
        return

    try:
        Db.init(__config)

    except Exception as ex:
        Dblog.err(ex, '[pyfetch init db] Error while initializing db from config.json') \
            .add('__config', __config) \
            .ok()
        print(f'[main.py]: failed: {ex}')

    __feed = data['slate_feed']

    try:
        print('Downloading AP scores from slate . . .')
        download_res = download_feed(__feed)

        if download_res is not None and 'success' in download_res:
            if download_res['success'] < 0:
                exit(download_res)

        print('Migrating exam recs to CX')

    except Exception as ex:
        Dblog.err(ex, '[pyfetch init] An unknown error occurred while running the dump.') \
            .add('__feed', __feed) \
            .add('__config', __config) \
            .ok()
        exit(-3)

    d = filter_data(__feed)

    if len(d['active']) > 0:
        res = migrate_to_cx(d['active'])

        updated = []
        inserted = []
        update_totals = {
            'success': 0,
            'skipped': 0
        }
        insert_totals = {
            'success': 0,
            'fail': 0
        }
        updated_persons = []
        inserted_persons = []

        for r in res:
            if 'operation' in r:
                if r['operation'] == 'update':

                    updated.append({
                        'rec': r['rec'],
                        'res': r['result']
                    })

                    if r['result'] > 0:
                        if 'rec' in r \
                                and '__r' in r['rec'] \
                                and id in r['rec']['__r'] \
                                and r['rec']['__r']['id'] in updated_persons:
                            inserted_persons.append(r['rec']['__r']['id'])
                        update_totals['success'] += 1

                    else:
                        update_totals['skipped'] += 1

                if r['operation'] == 'insert':

                    inserted.append({
                        'rec': r['rec'],
                        'res': r['result']
                    })

                    if r['result'] > 0:
                        if 'rec' in r \
                                and '__r' in r['rec'] \
                                and id in r['rec']['__r'] \
                                and r['rec']['__r']['id'] in inserted_persons:
                            inserted_persons.append(r['rec']['__r']['id'])
                        insert_totals['success'] += 1

                    else:
                        insert_totals['skipped'] += 1

        print('------')
        print(f'Total Inserted: {insert_totals}')
        print(f'Total Updated: {update_totals}')
        print('')
        # print(f'Total Individuals: {inserted_persons+updated_persons}')
        # print(f'Individuals Inserted: {inserted_persons}')
        # print(f'Individuals Updated: {updated_persons}')
        # print('')
        print(f'Total Non-active status: {len(d["bad_stat"])}')
        for bs in d["bad_stat"]:
            print(f'  --> {bs["name"]} - [{bs["_enr"]}]')
        print(f'Total No cxid: {d["no_cx"]}')
        print(f'Total No status: {len(d["no_stat"])}')


        #for ns in d["no_stat"]:
        #    print(ns['name'])

        #print('')
        #print('---')
        #_outp = ''
        #_missing_aps = []
        #for sk in res:
        #    _rec = sk['rec']
        #    _id = sk['rec']['name']
        #    _aps = sk['rec']['aps']
#
        #    if 'aps_unmapped' in _rec and len(_rec['aps_unmapped']) > 0 and _id not in _missing_aps:
        #        _missing = _rec['aps_unmapped']
        #        _outp += f'{_id} - \n'
        #        if _missing:
        #            for i, v in enumerate(_missing):
        #                _outp += f'  {i+1}. {v}\n'
        #            _outp += '\n'
        #            _missing_aps.append(_id)
#
        #print('---')
        #print(_outp)

    exit(0)


def migrate_to_cx(active):
    def make_migration_rec(rec, op, cxid, ctgry, score, res):
        return {
            'rec': rec,
            'operation': op,
            'cxid': cxid,
            'ctry': ctgry,
            'score': score,
            'result': res
        }

    results = []

    try:
        yr = Db.cx().scalar("execute procedure pccuryr()").strip()
        sess = Db.cx().scalar("execute procedure pccursess()").strip()

    except Exception:
        print('could not query for current session and year (pccuryr & pccursess)')

    for a in active:
        cxid = None
        enr = None

        if '__r' in a and a['__r']:
            cxid = a['__r']['id']

        if 'enr_code' in a:
            enr = a['enr_code']

        if 'aps' in a and a['aps']:

            for s in a['aps']:
                score1 = 0
                score2 = 0
                score3 = 0
                score4 = 0
                score5 = 0

                cat = s['code']
                remark = s['txt']

                # skip if score is not a valid #
                try:
                    score1 = int(s['score'].strip())
                except ValueError:
                    print(f'[skipping {cxid}] score1 `{s["score"]}` is not an integer')
                    continue

                if not cat.lower().startswith('ap'):
                    continue

                _cmdExists = 'select * from exam_rec where id = ? and ctgry = ?;'
                resExists = Db.cx().row(_cmdExists, (cxid, cat))

                if resExists is not None:  # update (found record)
                    _cmdUpdate = 'update exam_rec ' \
                                 'set ctgry = ?, score1 = ?, remark = ? ' \
                                 'where id = ? and score1 > ? and ctgry = ?;'
                    resUpdate = Db.cx().non_query(_cmdUpdate, (cat, score1, remark, cxid, score1, cat),
                                                  commit_query=True)

                    if resUpdate > 0:
                        results.append(make_migration_rec(a, 'update', cxid, cat, score1, resUpdate))
                        print(f'-> {cxid} - Update success: {resUpdate}, ({cat}) {score1} ')
                        print(f'--> cat: {cat}, sess: {sess}, yr: {yr}, score1: {score1}, cxid: {cxid}')
                        print(f'--> cmd: {_cmdUpdate}')
                    else:
                        results.append(make_migration_rec(a, 'update', cxid, cat, score1, resUpdate))
                        print(f'-> {cxid} Update skipped: {resUpdate}')
                        print(f'--> cat: {cat}, sess: {sess}, yr: {yr}, score1: {score1}, cxid: {cxid}')

                else:  # insert (no record)
                    _cmdIns = '''insert into 
                    exam_rec(id, ctgry, cmpl_date, score1, score2, score3, score4, score5, site, remark, self_rpt)
                    values (?, ?, SYSDATE, ?, ?, ?, ?, ?, 'CUC', ?, 'N')'''
                    res = Db.cx().non_query(_cmdIns,
                                            (cxid, cat, score1, score2, score3, score4, score5, remark),
                                            commit_query=True)

                    if res > 0:
                        results.append(make_migration_rec(a, 'insert', cxid, cat, score1, res))
                        print(f'-> {cxid} - Insert success ({cat}) {score1}')
                    else:
                        results.append(make_migration_rec(a, 'insert', cxid, cat, score1, res))
                        print(f'-> {cxid} Insert failed')

    return results


def filter_data(feed, enr_codes_allowed=['E', 'A', 'L', 'C', 'N']):
    table = feed['dump_table']
    # go to mapping table (ap_score_mappings)
    # get all the slate_fields column
    # download the dump_ap_raw data into python
    # check each row to see if there is a non-null value in slate_fields
    mappings = Db.forms().rows("select * from [ap_score_mappings]")
    slate = Db.forms().rows("select * from [dump_ap_raw]")

    recs = []
    no_cxid = 0
    unknown_fields = []

    if len(slate) > 0:
        _r = slate[0]
        for k in _r.keys():

            _unknown = True
            for m in mappings:
                _k = k.lower()

                if _k.startswith('ap') and _k.endswith('score'):
                    if _k.startswith(m['slate_fields'].lower()):
                        _unknown = False
                        break

            if _unknown:
                _f = k.lower()
                if _f.startswith('ap') and _f.endswith('score'):
                    unknown_fields.append({
                        'field': k,
                        'raw': _r
                    })

    if len(unknown_fields) > 0:
        print('[Warning] There are unmapped AP scores . . .')
        for u in unknown_fields:
            print(f" -> {u['field']}")

    for stu in slate:
        if 'CXID' in stu:
            cxid = stu['CXID']

            if cxid is None:
                no_cxid += 1
                continue

            # cxid
            __r = Db.cx().row(f'select * from id_rec where id = {cxid}')
            name = ''
            if __r is not None:
                name = str(__r['fullname']).strip()

            rec = {
                'name': f'{name} - {cxid}',
                'aps': [],
                '__r': __r,
                'stu': stu
            }

            # student stat
            _cmd = f'''
            select enr_tbl.txt enr_txt, enr_tbl.currenr enr_code
            from prog_enr_rec enr, currenr_table enr_tbl
            where enr.currenr = enr_tbl.currenr and enr.id = ?;'''
            __rEnr = Db.cx().row(_cmd, (cxid))

            stat_code = ''
            stat_txt = ''

            if __rEnr is not None:
                stat_code = str(__rEnr['enr_code']).strip()
                stat_txt = str(__rEnr['enr_txt']).strip()
                rec['enr'] = f'{stat_txt.strip()} ({stat_code.strip()})'
                rec['enr_code'] = stat_code.strip()
                rec['enr_txt'] = stat_txt.strip()
                rec['_enr'] = __rEnr

            unmapped_aps = []
            for u in unknown_fields:
                _u = u['field']
                if _u and stu[_u]:
                    unmapped_aps.append({ _u: stu[_u] })

            for m in mappings:
                _mfield = m['slate_fields']
                if _mfield in stu and stu[_mfield] is not None:
                    rec['aps'].append({
                        'descr': f"\t({m['cx']}) {_mfield}: {stu[_mfield]}",
                        'code': m['cx'],
                        'score': stu[_mfield],
                        'txt': f"{m['cx_txt']}",
                    })

            rec['aps_unmapped'] = unmapped_aps

            recs.append(rec)

    i = 0
    active = []
    no_stat = []
    bad_stat = []

    for r in recs:
        if len(r['aps']) > 0:

            if 'enr' in r:

                enr = r['enr_code'].upper().strip()
                if enr in enr_codes_allowed:
                    i += 1

                    active.append(r)
                    # print(f"{i}. {r['name']}")
                    # print(f"\tEnrollment: {r['enr']}")

                    # for ap in r['aps']:
                    #    print(ap['descr'])
                    # print('')

                else:
                    bad_stat.append(r)

            else:
                no_stat.append(r)

    return {
        'active': active,
        'no_stat': no_stat,
        'bad_stat': bad_stat,
        'no_cx': no_cxid
    }


def download_feed(feed):
    username = feed['username']
    password = feed['password']
    url = feed['url']
    table = feed['dump_table']

    global __download_if_new
    __download_if_new = feed['download_if_new']

    http = urllib3.PoolManager()
    headers = urllib3.util.make_headers(basic_auth=f'{username}:{password}')
    r = http.request('get', url, headers=headers)
    data = r.data.decode('utf8')

    if r.status != 200:
        return {
            'success': 0,
            'reason': f'[download_feed] expected return status of 200, but got {r.status}'
        }
    jdata = json.loads(data)
    if 'row' in jdata:
        entries = jdata['row']

        # create a super-set of all possible fields in the dataset
        cols = list(set(list(sub_col for col in entries for sub_col in list(col.keys()))))

        # [1] store the super-set, raw json data, sql template, and table name into _DownlogsLog
        #   *sql template is used in the next step to create a table to dump the data into
        store_res = store_download(entries, cols, table)

        # if it failed, return the code, and display the reason and the stacktrace
        if store_res['success'] < 0:
            if 'reason' in store_res:
                print(store_res['reason'])

            if 'error' in store_res:
                print(store_res['error'])

            return store_res

        # [2] create the table based on the sql template created in the previous step, this is pulled from _DownloadsLog
        print('[dump_ap_raw] Building Table...')
        create_table(store_res['content'])

        # [3] insert the data into the newly created table
        print(f'[dump_ap_raw] Inserting {len(entries)}...')
        insert_data(entries, table + '_raw')


def store_download(data, cols, tblname):
    id_hash_res = store_dump(data, cols, tblname)

    if id_hash_res['success'] == 1:
        print(f"stored as: {id_hash_res['content']}")
        return {'success': 1, 'content': id_hash_res['content']}

    else:
        return id_hash_res


def store_dump(data, cols, tblname):
    id_hash = f"%032x" % random.getrandbits(128)
    dl_bytes = bytes(str(data), 'utf-8')
    dl_hash = hashlib.sha1(dl_bytes).hexdigest()
    dl_res = Db.forms().scalar("select count(*) from _DownloadsLog where dl_hash = (?)", dl_hash)

    global __download_if_new
    if dl_res > 0 and __download_if_new:
        reason = '[store_dump] ignoring download since it exists if you want to proceed anyway, set download_is_new to true'
        return {'success': -1, 'reason': reason}

    cmdIns = 'insert into _DownloadsLog(tbl_name, cols, template, data, dl_hash, id_hash) values(?, ?, ?, ?, ?, ?)'
    template = make_template(cols)

    if template['success'] == 1:
        stemplate = json.dumps(template['content'])
        scols = json.dumps(cols)
        sdata = json.dumps(data)
        Db.forms().non_query(cmdIns, (tblname, scols, stemplate, sdata, dl_hash, id_hash))

    else:
        reason = f"{template['reason']}\n{template['error']}"
        return {'success': -2, 'reason': reason}

    return {'success': 1, 'content': id_hash}


def make_template(cols):
    template = []

    try:
        for i, k in enumerate(cols):
            itm = {
                'column': k,
                'display': k,
                'order': i
            }
            template.append(itm)

    except Exception as ex:
        reason = '[make_template] Error while constructing the string to make the table columns.'
        Dblog.err(ex, f'[pyfetch make_template] {reason}') \
            .add('template', template) \
            .add('cols', cols) \
            .add('__config', __config) \
            .ok()
        print(ex)
        return {'success': 0, 'error': ex, 'reason': reason}

    return {'success': 1, 'content': template}


def create_table(download_id=-1):
    cmd = "{ call dbo.spCreate_Table(?)  }"
    Db.forms().non_query(cmd, download_id)


def insert_data(data, tbl):
    def _m(res):
        print(res)

    Db.forms().batch_ins(tbl, data, _m)


if __name__ == '__main__':
    if sys.argv.__len__() > 1:
        __config = sys.argv[1]
    initialize()
