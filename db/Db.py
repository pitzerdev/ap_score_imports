import json
import socket

from db.DbConn import dbconn


class Db:
    _wd = None
    _cx = None
    _logs = None
    _dw = None
    _dw_dev = None
    _dw_slate = None
    _forms = None
    _formsdev = None

    @staticmethod
    def check_pzworkday():
        def _m(cnx):
            curs = cnx.cursor()
            curs.execute('select @@version')
            row = curs.fetchone()
            if len(row) > 0:
                print('[pzworkday] db is available')

        Db.wd().connect(_m)

    @staticmethod
    def init(path='config.json'):
        try:
            _host = socket.gethostname()

            data = json.load(open(path))
            Db._wd = data['db']['wd']
            Db._logs = data['db']['logs']
            Db._dw = data['db']['dw']
            Db._dw_dev = data['db']['dw_dev']
            Db._dw_slate = data['db']['dw_slate']
            Db._forms = data['db']['forms']
            Db._formsdev = data['db']['formsdev']

            if _host.lower() == 'pzworkday':
                Db._cx = data['db']['dsn_cx_prod']
                print(f'[CX Prod] Using dsn_cx_prod: {Db._cx}')
            else:
                Db._cx = data['db']['dsn_cx_dev']
                print(f'[CX Dev] Using dsn_cx_dev: {Db._cx}')

        except ConnectionError as e:
            print(f'[dbconn] Error {e.errno} {e.strerror}')
            raise

    @staticmethod
    def _get_wd():
        if Db._wd is None:
            Db.init()
        return Db._wd

    @staticmethod
    def _get_logs():
        if Db._logs is None:
            Db.init()
        return Db._logs

    @staticmethod
    def _get_cx():
        if Db._cx is None:
            Db.init()
        return Db._cx

    @staticmethod
    def wd():
        return dbconn(Db._get_wd())

    @staticmethod
    def cx():
        return dbconn(Db._get_cx())

    @staticmethod
    def logs():
        return dbconn(Db._get_logs())

    @staticmethod
    def _get_dw():
        if Db._dw is None:
            Db.init()
        return Db._dw

    @staticmethod
    def dw():
        return dbconn(Db._get_dw())

    @staticmethod
    def _get_dw_dev():
        if Db._dw_dev is None:
            Db.init()
        return Db._dw_dev

    @staticmethod
    def _get_dw_slate():
        if Db._dw_slate:
            Db.init()
        return Db._dw_slate

    @staticmethod
    def dw_slate():
        return dbconn(Db._get_dw_slate())

    @staticmethod
    def _get_forms():
        if Db._forms:
            Db.init()
        return Db._forms

    @staticmethod
    def forms():
        return dbconn(Db._get_forms())

    @staticmethod
    def _get_formsdev():
        if Db._formsdev:
            Db.init()
        return Db._formsdev

    @staticmethod
    def formsdev():
        return dbconn(Db._get_formsdev())
