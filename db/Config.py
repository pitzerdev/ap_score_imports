'''
This is a helper class to extract rows from the Work_Integration.config table.
'''
from db.Db import Db


class Config:
    _cmd = 'select * from config where category = ? and [name] = ?;'

    @staticmethod
    def get(category, name):
        def _m(res):
            if res is not None and 'value' in res:
                return res['value']
            else:
                return None

        return Db.wd().row(Config._cmd, (category, name), _m)
