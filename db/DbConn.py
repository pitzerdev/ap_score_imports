import pyodbc
import traceback

from helpers.Dblog import Dblog


class dbconn(object):
    data = None

    def __init__(self, db_data):
        self.data = db_data

    def connect(self, cb):
        cnx = None

        try:
            if self.data['type'] == 'informix':
                dsn = self.data["dsn"]
                cnx = pyodbc.connect(rf'DSN={dsn}', autocommit=False, timeout=600)

            else:
                server = self.data['host']
                database = self.data['database']
                uid = self.data['user']
                pwd = self.data['password']
                driver = '{SQL Server Native Client 11.0}'

                cnx = pyodbc.connect(
                    rf'DRIVER={driver};'
                    rf'SERVER={server};'
                    rf'DATABASE={database};'
                    rf'UID={uid};'
                    rf'PWD={pwd}'
                )

            return cb(cnx)

        except Exception as e:
            print(e)
            print(traceback.print_exc())
            raise

        finally:
            if cnx is not None:
                cnx.close()
            else:
                return None

    def rows(self, cmd, params=None, cb=None, commit=False, trim_cols=False):
        def _m(cnx):
            curs = cnx.cursor()
            if params is not None:
                curs.execute(cmd, params)
            else:
                curs.execute(cmd, ())
            rows = curs.fetchall()

            if commit:
                cnx.commit()

            if rows is not None:
                results = []
                cols = [col[0] for col in curs.description]
                for r in rows:
                    _r = dict()
                    for idx, c in enumerate(cols):
                        __col = r[idx]
                        if trim_cols and isinstance(__col, str):
                            __col = __col.strip()
                        _r[c] = __col
                    results.append(_r)

                if cb is not None:
                    return cb(results)
                else:
                    return results

            else:
                if cb is not None:
                    return cb(None)
                else:
                    return None

        return self.connect(_m)

    def row(self, cmd, params=None, cb=None, commit=False):
        def _m(cnx):
            curs = cnx.cursor()
            if params is None:
                curs.execute(cmd)
            else:
                curs.execute(cmd, params)

            r = curs.fetchone()

            if commit:
                cnx.commit()

            if r is not None:
                cols = [col[0] for col in curs.description]
                res = {}
                for idx, c in enumerate(cols):
                    res[c] = r[idx]

                if cb is not None:
                    return cb(res)
                else:
                    return res

            else:
                if cb is not None:
                    return cb(None)
                else:
                    return None

        return self.connect(_m)

    def scalar(self, cmd, params=None, cb=None):
        def _m(cnx):
            curs = cnx.cursor()
            if params is None:
                curs.execute(cmd)
            else:
                curs.execute(cmd, params)
            r = curs.fetchone()
            cnx.commit()

            if r is not None:
                cols = [col[0] for col in curs.description]

                if len(cols) == 0:
                    return None

                if cb is not None:
                    return cb(r[0])
                else:
                    return r[0]

            else:
                if cb is not None:
                    return cb(None)
                else:
                    return None

        return self.connect(_m)

    def non_query(self, cmd, params=None, commit_query=True, cb=None):
        def _m(cnx):
            curs = cnx.cursor()
            if params is None:
                curs.execute(cmd)
            else:
                curs.execute(cmd, params)

            if commit_query:
                cnx.commit()

            res = curs.rowcount

            if cb is not None:
                return cb(res)
            else:
                return res

        return self.connect(_m)

    def batch_ins(self, tbl, data, truncate_table=True, cb=None, query_type='mssql'):
        def _m(cnx):
            curs = cnx.cursor()
            failures = []
            success = []

            print(f'About to insert {len(data)} entries into {tbl}...')
            if len(data) > 0:

                if truncate_table:
                    if query_type == 'cx':
                        q = f'delete from {tbl}'
                    else:
                        q = f'truncate table {tbl}'
                    curs.execute(q)

            for d in data:
                cols = list(d.keys())
                vals = []

                qcols = ''
                qvals = ''

                for i, c in enumerate(cols):
                    if query_type == 'cx':
                        qcols += f'{c} '
                    else:
                        qcols += f'[{c}] '

                    qvals += f'? '
                    if i < len(cols) - 1:
                        qcols += ' , '
                        qvals += ' , '

                    v = d[c]
                    if isinstance(v, list):
                        v = None
                    vals.append(v)

                if query_type == 'cx':
                    q = f'insert into {tbl} ({qcols}) values ({qvals})'
                else:
                    q = f'insert into [{tbl}] ({qcols}) values ({qvals})'


                try:
                    curs.execute(q, vals)
                    success.append(d)

                except Exception as ex:
                    failures.append({
                        'data': d,
                        'exception': str(ex),
                        'stacktrace': str(traceback.print_exc()),
                        'cols': cols,
                        'vals': vals,
                        'qcols': qcols,
                        'qvals': qvals,
                        'q': q
                    })
                    title = f'[batch_ins] error executing on entry: {d}'
                    Dblog.err(ex, title)\
                        .add('d', d)\
                        .add('data', data)\
                        .add('tbl', tbl)\
                        .add('data', data)\
                        .add('failures', failures)\
                        .ok()
                    print(title)
                    print(ex)
                    print(traceback.print_exc())

            try:
                cnx.commit()
                print(f'Batch Insert to [{tbl}] complete.')

            except Exception as ex:
                cnx.rollback()
                title = f'[batch_ins] error committing on entry'
                Dblog.err(ex, title) \
                    .add('d', d) \
                    .add('data', data) \
                    .add('tbl', tbl) \
                    .add('data', data) \
                    .add('failures', failures) \
                    .ok()
                print(title)
                print(ex)
                print(traceback.print_exc())

            finally:
                res = {'fail': failures, 'success': success}
                if cb is not None:
                    return cb(res)
                else:
                    return res

        return self.connect(_m)
