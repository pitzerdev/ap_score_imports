import json
import socket



def logs():
    cmd = "select * from dblog"

    from db.Db import Db
    res = Db.logs().rows(cmd, ())
    return res


def _log(env, _type, title, data, aux):
    _env = ''
    app = ''
    sec = ''
    cmd = ''

    try:
        app = 'pzworkday'
        sec = 'integrations'
        cmd = "{ CALL spDblog_Insert (?, ?, ?, ?, ?, ?, ?) }"
        _env = f'{env} - {socket.gethostname()}'

        _data = json.dumps(data, default=str, sort_keys=False)
        _aux = json.dumps(aux, default=str, sort_keys=False)

        from db.Db import Db
        return Db.logs().non_query(cmd, (str(_env), str(_type), str(app), str(sec), str(title), _data, _aux))

    except Exception as ex:
        title = f'[Dblog Error] Error when writing to DB for {title}'
        #Dblog.err(ex, title)\
        #    .add('app', app)\
        #    .add('sec', sec)\
        #    .add('cmd', cmd)\
        #    .add('_type', _type)\
        #    .add('env', env)\
        #    .add('_env', _env)\
        #    .ok()
        print(title)
        print(ex)


def Warn(env: str, title: str, data: str, aux=None):
    return _log(env, 'warn', title, data, aux)


def Fatal(env: str, title: str, data: str, aux=None):
    return _log(env, 'fatal', title, data, aux)


def Error(env: str, title: str, data: str, aux=None):
    return _log(env, 'error', title, data, aux)


def Info(env: str, title: str, data: str, aux=None):
    return _log(env, 'info', title, data, aux)
